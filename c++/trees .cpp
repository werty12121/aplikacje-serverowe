#include <iostream>
#include <vector>

using namespace std;

class BTNode
{
  public:
	BTNode *up;
	BTNode *left;
	BTNode *right;
	int data;
	int minValue();
	BTNode *remove(int value, BTNode *parent);
};
unsigned log2(unsigned x)
{
	unsigned y = 1;

	while ((x >>= 1) > 0)
		y <<= 1;

	return y;
}
void rot_L(BTNode *&root, BTNode *node)
{
	BTNode *node2 = node->right, *p = node->up;

	if (node2 != NULL)
	{
		node->right = node2->left;
		if (node->right)
			node->right->up = node;

		node2->left = node;
		node2->up = p;
		node->up = node2;

		if (p)
		{
			if (p->left == node)
				p->left = node2;
			else
				p->right = node2;
		}
		else
		{
			root = node2;
		}
	}
}

void rot_R(BTNode *&root, BTNode *node)
{
	BTNode *node2 = node->left, *p = node->up;

	if (node2 != NULL)
	{
		node->left = node2->right;
		if (node->left)
			node->left->up = node;

		node2->right = node;
		node2->up = p;
		node->up = node2;

		if (p)
		{
			if (p->left == node)
				p->left = node2;
			else
				p->right = node2;
		}
		else
		{
			root = node2;
		}
	}
}
int DSW(BTNode *&nd)
{
	int n = 0;
	BTNode *tmp = nd;
	while (tmp != NULL)
	{
		if (tmp->left != NULL)
		{
			rot_L(tmp, tmp->left);
			tmp = tmp->up;
		}
		else
		{
			n++;
			tmp = tmp->right;
		}
	}
	unsigned s = -log2(n + 1) + n + 1;
	tmp = nd;
	for (int i = 0; i < s; i++)
	{
		rot_L(nd, tmp);
		tmp = tmp->up->right;
	}
	n = n - s;
	while (n > 1)
	{
		n--;
		tmp = nd;
		for (int i = 0; i < n; i++)
		{
			rot_L(nd, tmp);
			tmp = tmp->up->right;
		}
	}
}

BTNode *Search(BTNode *nd, int x)
{
	if (nd->data == x)
	{
		return nd;
	}
	if (x > nd->data)
	{
		if (nd->right != NULL)
		{
			return Search(nd->right, x);
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		if (nd->left != NULL)
		{
			return Search(nd->left, x);
		}
		else
		{
			return NULL;
		}
	}
}
int BTNode::minValue()
{
	if (this->left == NULL)
	{
		return this->data;
	}
	else
	{
		return this->left->minValue();
	}
}
BTNode *BTNode::remove(int value, BTNode *parent)
{
	cout << this->data << endl;
	if (value < this->data)
	{
		if (this->left != NULL)
		{
			return this->left->remove(value, this);
		}
		else
		{
			return NULL;
		}
	}
	else if (value > this->data)
	{
		if (this->right != NULL)
		{
			return this->right->remove(value, this);
		}
		else
		{
			return NULL;
		}
	}
	else
	{
		cout << "asasd" << endl;
		if (this->left != NULL && this->right != NULL)
		{
			cout << "asasd1" << endl;

			this->data = this->right->minValue();

			return this->right->remove(this->data, this);
		}
		else if (parent->left == this)
		{
			cout << "asasd2" << endl;
			if (this->left != NULL)
			{
				parent->left = this->left;
			}
			else
			{
				parent->left = this->right;
			}

			return this;
		}
		else if (parent->right == this)
		{
			cout << "asasd3" << endl;
			if (this->left != NULL)
			{
				parent->right = this->left;
			}
			else
			{
				parent->right = this->right;
			}

			return this;
		}
	}
}
void PreOrder(BTNode *nd)
{
	cout << nd->data << endl;
	if (nd->left != NULL)
	{
		PreOrder(nd->left);
	}
	if (nd->right != NULL)
	{
		PreOrder(nd->right);
	}
}
void InOrder(BTNode *nd)
{

	if (nd->left != NULL)
	{
		InOrder(nd->left);
	}
	cout << nd->data << endl;
	if (nd->right != NULL)
	{
		InOrder(nd->right);
	}
}
void PostOrder(BTNode *nd)
{
	if (nd->left != NULL)
	{
		PostOrder(nd->left);
	}
	if (nd->right != NULL)
	{
		PostOrder(nd->right);
	}
	cout << nd->data << endl;
}
void addnode(BTNode *&node, int x, BTNode *parent)
{
	BTNode *e = new BTNode;
	e->data = x;
	e->left = NULL;
	e->right = NULL;
	e->up = parent;
	if (node == NULL)
	{
		node = e;
	}
	else
	{
		if (x > node->data)
		{
			addnode(node->right, x, node);
		}
		else
		{
			addnode(node->left, x, node);
		}
	}
}

int main()
{
	int n;
	cin >> n;
	vector<int> tab;
	for (int i = 0; i < n; i++)
	{
		int d;
		cin >> d;
		tab.push_back(d);
	}
	BTNode *firstnode = NULL;
	for (int i = 0; i < n; i++)
	{
		addnode(firstnode, tab[i], NULL);
	}
	//PostOrder(Search(firstnode,4));
	PreOrder(firstnode);
	cout << endl;
	//	firstnode->remove(4,NULL);
	//	cout<<endl;
	//	PreOrder(firstnode);
	//	cout<<endl;
	DSW(firstnode);
	InOrder(firstnode);
}
