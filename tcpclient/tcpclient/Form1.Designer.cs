﻿namespace tcpclient
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.b_connect = new System.Windows.Forms.Button();
            this.nup_port = new System.Windows.Forms.NumericUpDown();
            this.tb_ip = new System.Windows.Forms.TextBox();
            this.tb_message = new System.Windows.Forms.TextBox();
            this.b_send = new System.Windows.Forms.Button();
            this.lv_messages = new System.Windows.Forms.ListView();
            this.bw_client = new System.ComponentModel.BackgroundWorker();
            this.ip_l = new System.Windows.Forms.Label();
            this.port_l = new System.Windows.Forms.Label();
            this.lnick = new System.Windows.Forms.Label();
            this.tbnick = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.nup_port)).BeginInit();
            this.SuspendLayout();
            // 
            // b_connect
            // 
            this.b_connect.Location = new System.Drawing.Point(12, 113);
            this.b_connect.Name = "b_connect";
            this.b_connect.Size = new System.Drawing.Size(378, 23);
            this.b_connect.TabIndex = 0;
            this.b_connect.Text = "connect";
            this.b_connect.UseVisualStyleBackColor = true;
            this.b_connect.Click += new System.EventHandler(this.button1_Click);
            // 
            // nup_port
            // 
            this.nup_port.Location = new System.Drawing.Point(43, 40);
            this.nup_port.Maximum = new decimal(new int[] {
            65565,
            0,
            0,
            0});
            this.nup_port.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nup_port.Name = "nup_port";
            this.nup_port.Size = new System.Drawing.Size(347, 20);
            this.nup_port.TabIndex = 1;
            this.nup_port.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nup_port.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // tb_ip
            // 
            this.tb_ip.Location = new System.Drawing.Point(43, 6);
            this.tb_ip.Name = "tb_ip";
            this.tb_ip.Size = new System.Drawing.Size(347, 20);
            this.tb_ip.TabIndex = 2;
            // 
            // tb_message
            // 
            this.tb_message.Location = new System.Drawing.Point(12, 245);
            this.tb_message.Name = "tb_message";
            this.tb_message.Size = new System.Drawing.Size(378, 20);
            this.tb_message.TabIndex = 3;
            this.tb_message.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // b_send
            // 
            this.b_send.Enabled = false;
            this.b_send.Location = new System.Drawing.Point(12, 274);
            this.b_send.Name = "b_send";
            this.b_send.Size = new System.Drawing.Size(378, 23);
            this.b_send.TabIndex = 4;
            this.b_send.Text = "send";
            this.b_send.UseVisualStyleBackColor = true;
            this.b_send.Click += new System.EventHandler(this.b_send_Click);
            // 
            // lv_messages
            // 
            this.lv_messages.GridLines = true;
            this.lv_messages.Location = new System.Drawing.Point(12, 142);
            this.lv_messages.Margin = new System.Windows.Forms.Padding(3, 0, 3, 3);
            this.lv_messages.Name = "lv_messages";
            this.lv_messages.Size = new System.Drawing.Size(378, 97);
            this.lv_messages.TabIndex = 5;
            this.lv_messages.TileSize = new System.Drawing.Size(350, 25);
            this.lv_messages.UseCompatibleStateImageBehavior = false;
            this.lv_messages.View = System.Windows.Forms.View.Tile;
            // 
            // bw_client
            // 
            this.bw_client.WorkerSupportsCancellation = true;
            this.bw_client.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bw_client_DoWork);
            // 
            // ip_l
            // 
            this.ip_l.AutoSize = true;
            this.ip_l.Location = new System.Drawing.Point(12, 9);
            this.ip_l.Name = "ip_l";
            this.ip_l.Size = new System.Drawing.Size(15, 13);
            this.ip_l.TabIndex = 6;
            this.ip_l.Text = "ip";
            this.ip_l.Click += new System.EventHandler(this.label1_Click);
            // 
            // port_l
            // 
            this.port_l.AutoSize = true;
            this.port_l.Location = new System.Drawing.Point(9, 42);
            this.port_l.Name = "port_l";
            this.port_l.Size = new System.Drawing.Size(25, 13);
            this.port_l.TabIndex = 7;
            this.port_l.Text = "port";
            // 
            // lnick
            // 
            this.lnick.AutoSize = true;
            this.lnick.Location = new System.Drawing.Point(12, 80);
            this.lnick.Name = "lnick";
            this.lnick.Size = new System.Drawing.Size(27, 13);
            this.lnick.TabIndex = 9;
            this.lnick.Text = "nick";
            this.lnick.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // tbnick
            // 
            this.tbnick.Location = new System.Drawing.Point(43, 77);
            this.tbnick.Name = "tbnick";
            this.tbnick.Size = new System.Drawing.Size(347, 20);
            this.tbnick.TabIndex = 8;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 333);
            this.Controls.Add(this.lnick);
            this.Controls.Add(this.tbnick);
            this.Controls.Add(this.port_l);
            this.Controls.Add(this.ip_l);
            this.Controls.Add(this.lv_messages);
            this.Controls.Add(this.b_send);
            this.Controls.Add(this.tb_message);
            this.Controls.Add(this.tb_ip);
            this.Controls.Add(this.nup_port);
            this.Controls.Add(this.b_connect);
            this.MaximumSize = new System.Drawing.Size(418, 371);
            this.MinimumSize = new System.Drawing.Size(418, 371);
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "ClientTCP";
            ((System.ComponentModel.ISupportInitialize)(this.nup_port)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button b_connect;
        private System.Windows.Forms.NumericUpDown nup_port;
        private System.Windows.Forms.TextBox tb_ip;
        private System.Windows.Forms.TextBox tb_message;
        private System.Windows.Forms.Button b_send;
        private System.Windows.Forms.ListView lv_messages;
        private System.ComponentModel.BackgroundWorker bw_client;
        private System.Windows.Forms.Label ip_l;
        private System.Windows.Forms.Label port_l;
        private System.Windows.Forms.Label lnick;
        private System.Windows.Forms.TextBox tbnick;
    }
}

