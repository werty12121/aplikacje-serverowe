﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace tcpclient
{
    public partial class Form1 : Form
    {
        private TcpClient client;
        private BinaryReader reading;
        private BinaryWriter writing;
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
                bw_client.RunWorkerAsync();
            b_send.Enabled = false;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void bw_client_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                client = new TcpClient(tb_ip.Text, (int)nup_port.Value);
                NetworkStream ns = client.GetStream();
                reading = new BinaryReader(ns);
                writing = new BinaryWriter(ns);
                writing.Write("pass");
                b_connect.Enabled = false;

            }
            catch
            {
            }
            b_send.Invoke(new MethodInvoker(delegate { b_send.Enabled = true; }));
            string message_recived;
            try
            {


                while ((message_recived = reading.ReadString()) != "END")
                {
                    lv_messages.Invoke(new MethodInvoker(delegate { lv_messages.Items.Add(message_recived); }));
                    lv_messages.Invoke(new MethodInvoker(delegate { lv_messages.EnsureVisible(lv_messages.Items.Count - 1); }));
                }
                client.Close();
                bw_client.CancelAsync();
            }
            catch {
                MessageBox.Show("server disconnected");
                b_connect.Invoke(new MethodInvoker(delegate { b_connect.Enabled = true; }));
                b_send.Invoke(new MethodInvoker(delegate { b_send.Enabled = false; }));
                client.Close();
                bw_client.CancelAsync();
            }
        }

        private void b_send_Click(object sender, EventArgs e)
        {
            try
            {
                
                if (tb_message.Text != "END")
                {
                    writing.Write(DateTime.Now.ToString("h:mm:ss tt") + "  <"+tbnick.Text+">: "+tb_message.Text);
                }
                else
                {
                    bw_client.CancelAsync();
                    client.Close();
                }
            }
            catch {
                bw_client.CancelAsync();
                client.Close();
                b_send.Enabled = false;
            }
           
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }
    }
}
