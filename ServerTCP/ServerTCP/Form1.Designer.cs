﻿namespace ServerTCP
{
    partial class Form1
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Wymagana metoda obsługi projektanta — nie należy modyfikować 
        /// zawartość tej metody z edytorem kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.bStrart = new System.Windows.Forms.Button();
            this.lAddress = new System.Windows.Forms.Label();
            this.lPort = new System.Windows.Forms.Label();
            this.tbAddress = new System.Windows.Forms.TextBox();
            this.nupPort = new System.Windows.Forms.NumericUpDown();
            this.lvLog = new System.Windows.Forms.ListView();
            this.bwConnection = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.nupPort)).BeginInit();
            this.SuspendLayout();
            // 
            // bStrart
            // 
            this.bStrart.Location = new System.Drawing.Point(17, 115);
            this.bStrart.Name = "bStrart";
            this.bStrart.Size = new System.Drawing.Size(284, 23);
            this.bStrart.TabIndex = 0;
            this.bStrart.Text = "Start";
            this.bStrart.UseVisualStyleBackColor = true;
            this.bStrart.Click += new System.EventHandler(this.bStrart_Click);
            // 
            // lAddress
            // 
            this.lAddress.AutoSize = true;
            this.lAddress.Location = new System.Drawing.Point(14, 14);
            this.lAddress.Name = "lAddress";
            this.lAddress.Size = new System.Drawing.Size(18, 13);
            this.lAddress.TabIndex = 2;
            this.lAddress.Text = "ip:";
            // 
            // lPort
            // 
            this.lPort.AutoSize = true;
            this.lPort.Location = new System.Drawing.Point(150, 14);
            this.lPort.Name = "lPort";
            this.lPort.Size = new System.Drawing.Size(25, 13);
            this.lPort.TabIndex = 3;
            this.lPort.Text = "port";
            // 
            // tbAddress
            // 
            this.tbAddress.Location = new System.Drawing.Point(44, 11);
            this.tbAddress.Name = "tbAddress";
            this.tbAddress.Size = new System.Drawing.Size(100, 20);
            this.tbAddress.TabIndex = 4;
            // 
            // nupPort
            // 
            this.nupPort.Location = new System.Drawing.Point(181, 12);
            this.nupPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
            this.nupPort.Name = "nupPort";
            this.nupPort.Size = new System.Drawing.Size(120, 20);
            this.nupPort.TabIndex = 5;
            this.nupPort.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // lvLog
            // 
            this.lvLog.Location = new System.Drawing.Point(17, 38);
            this.lvLog.Name = "lvLog";
            this.lvLog.Size = new System.Drawing.Size(284, 71);
            this.lvLog.TabIndex = 6;
            this.lvLog.UseCompatibleStateImageBehavior = false;
            this.lvLog.View = System.Windows.Forms.View.Tile;
            // 
            // bwConnection
            // 
            this.bwConnection.WorkerSupportsCancellation = true;
            this.bwConnection.DoWork += new System.ComponentModel.DoWorkEventHandler(this.bwConnection_DoWork);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.ClientSize = new System.Drawing.Size(321, 146);
            this.Controls.Add(this.lvLog);
            this.Controls.Add(this.nupPort);
            this.Controls.Add(this.tbAddress);
            this.Controls.Add(this.lPort);
            this.Controls.Add(this.lAddress);
            this.Controls.Add(this.bStrart);
            this.Name = "Form1";
            this.Text = "ServerTCP";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.nupPort)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button bStrart;
        private System.Windows.Forms.Label lAddress;
        private System.Windows.Forms.Label lPort;
        private System.Windows.Forms.TextBox tbAddress;
        private System.Windows.Forms.NumericUpDown nupPort;
        private System.Windows.Forms.ListView lvLog;
        private System.ComponentModel.BackgroundWorker bwConnection;
    }
}

