﻿using System;


using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using sklep;
using sklep.Helpers;

public partial class Product : Page
{
    private sklep.Helpers.Product page_product;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] == null)
        {
            Response.Redirect("~/");
        }
        insertdata();
    }
    private void insertdata() {

        page_product = new sklep.Helpers.Product();
        page_product.ID = Request.QueryString["id"];
        page_product.updateStatus();
        Price.Text = page_product.price;
        Quantity.Text = page_product.quantity;
        Producent.Text = page_product.producent;
        ProductImage.ImageUrl = page_product.img;
        Name.Text = page_product.name;
        Descripction.Text = page_product.descripction;
       
    }
    protected void kupb_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("~/Basket?add=" + page_product.ID);
    }

    protected void uploadcomment_Click(object sender, EventArgs e)
    {
        Rating newr = new Rating();
        newr.comment = comment.Text;
        if (int.Parse(mark.Text) > 5) {
            mark.Text = "5";
        }
        if (int.Parse(mark.Text) < 0)
        {
            mark.Text = "0";
        }
            newr.mark = mark.Text;
        newr.product = sklep.Helpers.Product.getProductById(Request.QueryString["id"]);
        if (Session["userlogin"] != null)
        {
            newr.userlogin = Session["userlogin"].ToString();
        }
        else {
            newr.userlogin = "anonymus";
        }
        newr.insert();
        ratingsdataslist.DataBind();
    }
}