﻿using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using System;
using System.Web;
using System.Web.UI;
using sklep;
using sklep.Helpers;

public partial class Account_Login : Page
{
        protected void Page_Load(object sender, EventArgs e)
        {
        errorlabel.Text = "";
            if (Session["userid"] != null)
            {
                Response.Redirect("~/");
            }
        }

        protected void LogIn(object sender, EventArgs e)
        {
            if (IsValid){
            User temp=sklep.Helpers.User.login(UserName.Text, Password.Text);
            if (temp != null)
            {
                Session["userid"] = temp.ID;
                Session["userlogin"] = temp.Login;
                Response.Redirect("~/");
            }
            else {
                errorlabel.Text = "Login lub Hasło niepoprawne.";
            }

            }
        }
}