﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.UI;
using sklep;
using sklep.Helpers;

public partial class Account_Register : Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] != null)
        {
            Response.Redirect("~/");
        }
    }
    protected void CreateUser_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            User newuser = new User();
            newuser.Login = UserName.Text;
            newuser.Password = Password.Text;
            newuser.Email = Email.Text;
            newuser.City = City.Text;
            newuser.Street = Street.Text;
            newuser.PostCode = PostCode.Text;
            newuser.insert();
            if (newuser.ID != (-1).ToString())
            {
                Session["userid"] = newuser.ID;
                Session["userlogin"] = newuser.Login;
            }

            Response.Redirect("~/");
        }
       
    }
}