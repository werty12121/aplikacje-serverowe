﻿<%@ Page Title="Zakontem" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Manage.aspx.cs" Inherits="Account_Manage" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    

    

    <div class="row">
        <div class="col-md-12">
            <section id="dataedit">
                 <asp:PlaceHolder runat="server" ID="changePasswordHolder" Visible="true">
                    <div class="form-horizontal">
                        <h4>Formularz zmiany hasła</h4>
                        <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword" CssClass="col-md-2 control-label">Bieżące hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="CurrentPassword" TextMode="Password" CssClass="form-control" MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="CurrentPassword"
                                    CssClass="text-danger" ErrorMessage="Pole bieżącego hasła jest wymagane."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword" CssClass="col-md-2 control-label">Nowe hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" CssClass="form-control" MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPassword"
                                    CssClass="text-danger" ErrorMessage="Nowe hasło jest wymagane."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword" CssClass="col-md-2 control-label">Potwierdź nowe hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" CssClass="form-control" MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Potwierdzenie nowego hasła jest wymagane."
                                    ValidationGroup="ChangePassword" />
                                <asp:CompareValidator runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Nowe hasło i jego potwierdzenie są niezgodne."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" Text="Zmień hasło" OnClick="ChangePassword_Click" CssClass="btn btn-default" ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" ID="updateadres" Visible="true">

                    <div class="form-horizontal">
                        <h4>Formularz zmiany adresu</h4>
                        <hr />
                        <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="miasto" CssClass="col-md-2 control-label">Miasto</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="miasto"  CssClass="form-control"  MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="miasto"
                                    CssClass="text-danger" ErrorMessage="Miasto jest wymagane."
                                    Display="Dynamic" ValidationGroup="updateadres" />
                                <asp:ModelErrorMessage runat="server" ModelStateKey="miasto" AssociatedControlID="miasto"
                                    CssClass="text-danger" SetFocusOnError="true" />
                            </div>
                        </div>

                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="ulica" CssClass="col-md-2 control-label">Ulica</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="ulica" CssClass="form-control"  MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ulica"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Ulica jest wymagana"
                                    ValidationGroup="updateadres" />
                            </div>
                        </div>

                         <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="postcode" CssClass="col-md-2 control-label">Kod pocztowy</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="postcode" CssClass="form-control"  MaxLength="120" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="postcode"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Kod jest wymagana"
                                    ValidationGroup="updateadres" />
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" Text="Zmień adres" ValidationGroup="updateadres" OnClick="updateadres_Click" CssClass="btn btn-default" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>

            </section>
        </div>
    </div>
    <div class="row">
        <asp:ListView ID="orders" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <div>
                    <table>
                        <tr>
                            <td><%#"Id zamówienia:"+ Eval("id") %></td>
                            <td><%#"Status zamówienia:"+ Eval("status") %></td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:ListView>

        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT * FROM orders WHERE id=-1"></asp:SqlDataSource>

    </div>

</asp:Content>
