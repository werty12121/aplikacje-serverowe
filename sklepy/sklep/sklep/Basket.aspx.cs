﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using sklep.Helpers;

public partial class Basket : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        List<string> basket;
        if (Request.QueryString["add"] != null)
        {
            
            if (Session["basket"] != null)
                basket = Session["basket"].ToString().Split('-').ToList<string>();
            else
                basket = new List<string>();
            basket.Add(Request.QueryString["add"]);
            Session["basket"]=String.Join("-", basket.ToArray());
            Response.Redirect("~/");
        }
        if (Request.QueryString["del"] != null){
            if (Session["basket"] != null)
            {
                basket = Session["basket"].ToString().Split('-').ToList<string>();
                basket.RemoveAt(basket.FindIndex(x=>x==Request.QueryString["del"]));
                if (basket.Count == 0)
                {
                    Session["basket"] = null;
                }
                else {
                    Session["basket"] = String.Join("-", basket.ToArray());
                }
               
                Response.Redirect("~/Basket");
            }
        }
        if (Session["basket"] != null)
        {
            basket = Session["basket"].ToString().Split('-').ToList<string>();
            string ws = "";
            for(int i = 0; i < basket.Count(); i++)
            {
                if (i != 0)
                {
                    ws += " OR ";
                }
                ws += "id=" + basket[i];
            }
            SqlDataSource1.SelectCommand = "SELECT * FROM products WHERE " + ws;
            
            basketlist.DataBind();
        }
        else {
            tl.Text = "Twój koszyk jest pusty";
            SqlDataSource1.SelectCommand = "SELECT * FROM products WHERE id='-1'";
            basketlist.DataBind();
        }
        if (Session["userid"] != null)
        {
            anonymus.Visible = false;
        }
        else
        {
            logged.Visible = false;
        }
    }

    protected void zambutt_Click(object sender, EventArgs e)
    {
        if (Session["basket"] != null)
        {
            List<string> basket = Session["basket"].ToString().Split('-').ToList<string>();
            List<sklep.Helpers.Product> tpr = new List<sklep.Helpers.Product>();
            tl.Text = basket[0];
            for (int i = 0; i < basket.Count(); i++)
            {
                sklep.Helpers.Product p = sklep.Helpers.Product.getProductById(int.Parse(basket[i]).ToString());
                if (int.Parse(p.quantity) >= 1)
                {
                    tpr.Add(p);
                    p.quantity= (int.Parse(p.quantity)-1).ToString();
                    p.update();
                }
               
            }
            Order temp = new Order();
            temp.status = "new";
            temp.products = tpr;
            temp.user = sklep.Helpers.User.getUserById(Session["userid"].ToString());
            temp.insert();
            //Response.Redirect("~/Basket");
        }
    }
}