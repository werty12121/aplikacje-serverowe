﻿<%@ Page Title="Basket" Language="C#" AutoEventWireup="true" CodeFile="Basket.aspx.cs" Inherits="Basket" MasterPageFile="~/Site.master"%>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <asp:Label ID="tl" runat="server" Text="Label"></asp:Label>
        <asp:ListView ID="basketlist" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <div class="basketitem">
                    <table>
                        <tr>
                            <td>
                                <img src="<%# Eval("img")%>" />
                            </td>
                            <td>
                                <%# Eval("name")%>
                            </td>
                            <td>
                                <%# Eval("price")%>
                            </td>
                            <td>
                                <a href=<%#"/Basket?del="+  Eval("id")%> >Usuń</a>
                            </td>
                        </tr>
                    </table>
                </div>
            </ItemTemplate>
        </asp:ListView>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT * FROM products"></asp:SqlDataSource>
    </div>
    <div runat="server" ID="anonymus">
        <p>Musisz się zalogować alby dokonać zamówienia</p>
    </div>
    <div runat="server" ID="logged">
        <asp:Button ID="zambutt" runat="server" Text="Zamawiam" OnClick="zambutt_Click" />
    </div>
</asp:Content>