﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Admin_Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] == null&&Session["userlogin"].ToString()!="admin") {
            Response.Redirect("~/");
        }
    }
    protected void lbInsert_Click(object sender, EventArgs e)
    {
        try
        {
            SqlDataSource1.Insert();
        }
        catch (Exception exe) { }
    }
    protected void odsIngredient_Inserting(object sender, SqlDataSourceCommandEventArgs e)
    {
        try
        {
            string username = ((TextBox)gvUsers.FooterRow.FindControl("tbusername")).Text;
        string email = ((TextBox)gvUsers.FooterRow.FindControl("tbemail")).Text;
        string passwpord = ((TextBox)gvUsers.FooterRow.FindControl("tbpassword")).Text;
        string city = ((TextBox)gvUsers.FooterRow.FindControl("tbcity")).Text;
        string street = ((TextBox)gvUsers.FooterRow.FindControl("tbstreet")).Text;
        string postcode = ((TextBox)gvUsers.FooterRow.FindControl("tbpostcode")).Text;
        e.Command.Parameters["username"].Value = username;
        e.Command.Parameters["email"].Value = email;
        e.Command.Parameters["password"].Value = passwpord;
        e.Command.Parameters["city"].Value = city;
        e.Command.Parameters["street"].Value = street;
        e.Command.Parameters["postcode"].Value = postcode;
        }
        catch (Exception exe) { }
    }
    protected void insertproduct(object sender, EventArgs e)
    {
        try
        { 
            SqlDataSource2.Insert();
        }
        catch (Exception exe) { }

    }
    protected void odsIngredient_Inserting2(object sender, SqlDataSourceCommandEventArgs e)
    {
        try
        {
            string name = ((TextBox)GridView1.FooterRow.FindControl("tbname")).Text;
            string quantity = ((TextBox)GridView1.FooterRow.FindControl("tbquantity")).Text;
            string price = ((TextBox)GridView1.FooterRow.FindControl("tbprice")).Text;
            string img = ((TextBox)GridView1.FooterRow.FindControl("tbimg")).Text;
            string descripction = ((TextBox)GridView1.FooterRow.FindControl("tbdescripction")).Text;
            string producent = ((TextBox)GridView1.FooterRow.FindControl("tbproducent")).Text;
            e.Command.Parameters["name"].Value = name;
            e.Command.Parameters["quantity"].Value = quantity;
            e.Command.Parameters["price"].Value = price;
            e.Command.Parameters["img"].Value = img;
            e.Command.Parameters["descripction"].Value = descripction;
            e.Command.Parameters["producent"].Value = producent;
            e.Command.Parameters["category"].Value = 0;
        }
        catch (Exception exe) { }
        
    }
    protected void insertorder(object sender, EventArgs e)
    {
        try
        {
            SqlDataSource3.Insert();
        }
        catch (Exception exe) { }

    }
    protected void odsIngredient_Inserting3(object sender, SqlDataSourceCommandEventArgs e)
    {
        try
        {
            string user = ((TextBox)GridView2.FooterRow.FindControl("tbuser")).Text;
            string status = ((TextBox)GridView2.FooterRow.FindControl("tbstatus")).Text;
            string products = ((TextBox)GridView2.FooterRow.FindControl("tbproducts")).Text;
            e.Command.Parameters["user"].Value = user;
            e.Command.Parameters["status"].Value = status;
            e.Command.Parameters["products"].Value = products;

    }
        catch (Exception exe) { }

    }
}