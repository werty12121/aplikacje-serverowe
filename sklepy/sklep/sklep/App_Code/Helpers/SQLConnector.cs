﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Opis podsumowujący dla SQLConnector
/// </summary>
namespace sklep.Helpers
{
    public partial class SQLConnector
    {
        public SQLConnector()
        {
            //
            // TODO: Tutaj dodaj logikę konstruktora
            //
        }
        MySqlConnection connect()
        {
            string myconnection =
               "SERVER=localhost;" +
               "DATABASE=sklep;" +
               "UID=root;" +
               "PASSWORD=;";

            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {

                connection.Open();
                Console.WriteLine("Connected");
                return connection;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }
        public void updateOrDelete(string Query)
        {
            MySqlConnection conn = this.connect();
            if (conn == null) return;
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;
            command.ExecuteNonQuery();
            return;
        }
        public int insert(string Query)
        {

            MySqlConnection conn = this.connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;// "INSERT INTO table (column1, column2, column3,...) VALUES ('" + value1 + "', '" + value2 + "', '" + value2 + "')";
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e) {
                return -1;
            }
            
            return (int)command.LastInsertedId;
            
        }

        public MySqlDataReader reader(string Query)
        {
            MySqlConnection conn = connect();
            if (conn == null) return null;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = Query;// "SELECT * FROM table WHERE column1='" + value1 + "' AND column2='" + value2 + "' OR column3='" + value3 + "'";
            MySqlDataReader reader = command.ExecuteReader();
           
            return reader;
        }
    }
}
