﻿using MySql.Data.MySqlClient;
using sklep.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Opis podsumowujący dla Product
/// </summary>
namespace sklep.Helpers
{
    public class Product:MYSQLItem
    {
        public string ID { get; set; }
        public string quantity { get; set; }
        public string price { get; set; }
        public string img { get; set; }
        public string name { get; set; }
        public string descripction { get; set; }
        public string producent { get; set; }
        public Category category { get; set; }
        private SQLConnector con;
        public Product()
        {
            con = new SQLConnector();
        }
        public override void insert()
        {
            this.ID = con.insert("INSERT INTO products (quantity, price, img, name, descripction, producent, category) VALUES ('" + this.quantity + "', '" + this.price + "', '" + this.img + "', '" + this.name + "', '" + this.descripction + "', '" + this.producent + "', '" + this.category.ID + "')").ToString();
        }
        public override void update()
        {
            con.updateOrDelete("UPDATE products SET quantity='" + this.quantity + "', price='" + this.price + "', img='" + this.img + "', name='" + this.name + "', descripction='" + this.descripction + "', producent='" + this.producent + "', category='" + this.category.ID + "' WHERE id='" + this.ID + "'");
        }
        public override void delete()
        {
            con.updateOrDelete("DELETE FROM products WHERE id='" + this.ID + "'");
        }
        public override void updateStatus()
        {
            MySqlDataReader reader = con.reader("SELECT * FROM products WHERE id='" + this.ID + "'");
            while (reader.Read())
            {
                this.quantity = (string)reader["quantity"].ToString();
                this.price = (string)reader["price"].ToString();
                this.img = (string)reader["img"].ToString();
                this.name = (string)reader["name"].ToString();
                this.descripction = (string)reader["descripction"].ToString();
                this.producent = (string)reader["producent"].ToString();
                this.category = null;
            }
        }
        public static Product getProductById(string Id)
        {
            Product resoult = new Product();
            resoult.ID = Id;
            resoult.updateStatus();
            return resoult;
        }
    }
}