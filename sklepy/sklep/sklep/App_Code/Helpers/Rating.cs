﻿using MySql.Data.MySqlClient;
using sklep.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Opis podsumowujący dla Rating
/// </summary>
namespace sklep.Helpers
{
    public class Rating:MYSQLItem
    {
        public string userlogin { get; set; }
        public string mark { get; set; }
        public string ID { get; set; }
        public string comment { get; set; }
        public Product product { get; set; }
        private SQLConnector con;
        public Rating()
        {
            con = new SQLConnector();
        }
        public override void insert()
        {
            this.ID = con.insert("INSERT INTO ratings (product, mark, user, comment) VALUES ('" + this.product.ID + "', '" + this.mark + "', '" + this.userlogin +"', '"+this.comment+ "')").ToString();
            //con.insert(this.ID.ToString(), this.Login, this.Password);
        }
        public override void update()
        {
            con.updateOrDelete("UPDATE ratings SET product='" + this.product.ID + "', mark='" + this.mark + "', comment='" + this.comment + "', user='" + this.userlogin + "' WHERE id='" + this.ID + "'");
        }
        public override void delete()
        {
            con.updateOrDelete("DELETE FROM ratings WHERE id='" + this.ID + "'");
        }
        public override void updateStatus()
        {
            MySqlDataReader reader = con.reader("SELECT * FROM ratings WHERE id='" + this.ID + "'");
            while (reader.Read())
            {
                this.product = Product.getProductById((string)reader["product"]);
                this.mark = reader["mark"].ToString();
                this.userlogin = reader["user"].ToString();
                this.comment=reader["comment"].ToString();
            }
        }
    }
}