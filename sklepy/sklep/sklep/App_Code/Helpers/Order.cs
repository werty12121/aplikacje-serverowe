﻿using MySql.Data.MySqlClient;
using sklep.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Data klasa dla zamuwienia
/// </summary>
namespace sklep.Helpers
{
    public class Order :MYSQLItem
    {
        public User user { get; set; }
        public List<Product> products { get; set; }
        public string status { get; set; }
        public string ID { get; set; }
        private SQLConnector con;
        public Order()
        {
            con = new SQLConnector();
        }
        public override void insert()
        {
            string products_s = "";
            for (int i = 0; i < this.products.Count(); i++)
            {
                if (i != 0)
                {
                    products_s += "-";
                }
                products_s += "-" + this.products[i].ID;
            }
            this.ID = con.insert("INSERT INTO orders (user, products, status) VALUES ('" + this.user.ID + "', '" + products_s + "', '" + this.status + "')").ToString();
            //con.insert(this.ID.ToString(), this.Login, this.Password);
        }
        public override void update()
        {
            string products_s = "";
            for (int i = 0; i < this.products.Count(); i++)
            {
                if (i != 0)
                {
                    products_s += "-";
                }
                products_s += "-" + this.products[i].ID;
            }
            con.updateOrDelete("UPDATE orders SET user='" + this.user.ID + "', products='" + products_s + "', status='" + this.status + "' WHERE id='" + this.ID + "'");
        }
        public override void delete()
        {
            con.updateOrDelete("DELETE FROM orders WHERE id='" + this.ID + "'");
        }
        public override void updateStatus()
        {
            MySqlDataReader reader = con.reader("SELECT * FROM orders WHERE id='" + this.ID + "'");
            while (reader.Read())
            {
                this.user = User.getUserById((string)reader["user"].ToString());
                String[] pid = reader["products"].ToString().Split(',');
                this.products = new List<Product>();
                for (int i = 0; i < pid.Count(); i++)
                {
                    this.products.Add(Product.getProductById(this.ID));
                }
                this.status = (string)reader["status"];
            }
        }
    }
}