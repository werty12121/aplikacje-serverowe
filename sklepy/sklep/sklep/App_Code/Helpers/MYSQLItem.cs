﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sklep.Helpers
{
    /// <summary>
    /// Opis podsumowujący dla MYSQLItemcs
    /// </summary>
    public abstract class MYSQLItem
    {
        public abstract void insert();
        public abstract void update();
        public abstract void delete();
        public abstract void updateStatus();
    }
}
