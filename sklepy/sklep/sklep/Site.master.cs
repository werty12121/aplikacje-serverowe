﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{

    protected void Page_Init(object sender, EventArgs e)
    {
       
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["userid"] == null)
        {
            logged.Visible = false;
            anonymus.Visible = true;
        }
        else {
            logged.Visible = true;
            anonymus.Visible = false;
        }

    }

    protected void Logout(object sender, EventArgs e)
    {
        Session["userid"] = null;
        Session["userlogin"] = null;
    }
}