﻿<%@ Page Title="Product" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Product.aspx.cs" Inherits="Product" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="row">
        <div class="col">
        <asp:Image ID="ProductImage" runat="server" /><br />
        <asp:Label ID="Name" runat="server" Text="Label"></asp:Label><br />
        <asp:Label ID="Descripction" runat="server" Text="Label"></asp:Label><br />
         <asp:Label ID="Producent" runat="server" Text="Label"></asp:Label><br />
        <asp:Label ID="Price" runat="server" Text="Label"></asp:Label><br />
        <asp:Label ID="Quantity" runat="server" Text="Label"></asp:Label><br />
        <asp:ImageButton ID="kupb" runat="server" CssClass="cartbuttonimage" ImageUrl="http://freeflaticons.com/wp-content/uploads/2014/09/shopping-cart-free-commerce-icons-1410266092ngk48.png" OnClick="kupb_Click" />
       </div>
    </div>
     <div class="row">
         <asp:TextBox ID="mark" runat="server" TextMode="Number" Width="182px"></asp:TextBox><br />
         <asp:TextBox ID="comment" runat="server" Height="96px" MaxLength="200" TextMode="MultiLine" Width="466px"></asp:TextBox>
         <asp:Button ID="uploadcomment" runat="server" Text="Upload Rating" OnClick="uploadcomment_Click" />
     </div>
    <div class="row">
       
        <div class="col">
        <asp:DataList ID="ratingsdataslist" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <p>User: <%# Eval("user") %>,Mark: <%# Eval("mark") %> /5</p>
                <p>Comment: <%# Eval("comment") %> </p>
            </ItemTemplate>

        </asp:DataList>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT mark, comment, user FROM ratings WHERE (product = ?)">
                <SelectParameters>
                    <asp:QueryStringParameter DefaultValue="0" Name="id" QueryStringField="id" Type="Int32" />
                </SelectParameters>
            </asp:SqlDataSource>
            </div>
    </div>
    
</asp:Content>
