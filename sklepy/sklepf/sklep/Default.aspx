﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">

    <div class="row">
    <asp:ListView ID="lvprod" runat="server" DataSourceID="SqlDataSource1" EditIndex="1">
        <LayoutTemplate>
        <div id="itemPlaceholder" style="float:left" runat="server"></div>
    </LayoutTemplate>
        <ItemTemplate>
            <div style="float:left; margin:40px; text-align:center">
                <table>
                    <tr>
                        <td><img class="pic" src="<%# Eval("img")%>" /></td>
                    </tr>
                    <tr>
                        <td>Nazwa przedmiotu: <%# Eval("name")%></td>
                    </tr>
                    <tr>
                        <td>Producent: <%# Eval("producent")%></td>
                    </tr>
                    <tr>
                        <td>Cena: <%# Eval("price")%> [zł]</td>
                    </tr>
                    <tr>
                        <td>
                            <a runat="server" href=<%# "~/produkt?id="+Eval("id")%>>Szczegóły</a>
                        </td>
                        <td>
                            <a runat="server" href="/"><img class="cbimg" src="https://png.pngtree.com/element_pic/17/03/19/e5640c684740cc3525222aae0fd9c01f.jpg" /></a> 
                        </td>
                    </tr>
                    
                </table>
            </div>
        </ItemTemplate>
    </asp:ListView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT * FROM products"></asp:SqlDataSource>
    </div>










</asp:Content>
