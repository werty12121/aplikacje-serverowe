﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="users.aspx.cs" Inherits="administracja_users" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:GridView ShowFooter="True" DataKeyNames="id" AutoGenerateColumns="False" ID="gvUsers" runat="server" DataSourceID="UsersSqlDataSource" BackColor="White" BorderColor="#DEDFDE" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Vertical">
        <AlternatingRowStyle BackColor="White" />
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:TemplateField HeaderText="username" SortExpression="username">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("username") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("username") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="tbusername" runat="server" Text='<%# Bind("username") %>'></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="email" SortExpression="email">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("email") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="tbemail" runat="server" Text='<%# Bind("email") %>'></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="password" SortExpression="password">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("password") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("password") %>'></asp:Label>
                </ItemTemplate>
                <FooterTemplate>
                    <asp:TextBox ID="tbpassword" runat="server" Text='<%# Bind("password") %>'></asp:TextBox>
                </FooterTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
               
                <FooterTemplate>
                    <asp:ImageButton ImageUrl="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAY1BMVEX///8AAABHR0eTk5P8/Pzg4ODb29uLi4vr6+sZGRmoqKhRUVH09PR2dna6urq9vb2Xl5eCgoKvr6+hoaHLy8sJCQkTExM9PT1gYGBwcHAlJSVXV1fp6elmZmY3Nzd7e3svLy//oKi/AAAI0klEQVR4nOVd2YKrIAydumtduri0tcv8/1fesZ1ONxNAE+TieZxFOAokOQnw9cUNLw2KZOtmbfN9iOtqUdXx4btpM3ebFEHqsbfPicgvwr1TLTBUzj4s/Gjqrg5AkGdOjHJ7RuxkeTB1lxXgJ7tamtwD9S7xp+66BNJyLf/pej7mukynpoAhyk8j2N1xyk2dlsUJX1TkUZ2Kqcl8wl8TsbtjbdScTDcOMb8OzsaUKRm5Y9YWDLFrwoz0MyZ6N2RTD9Zgx8qvw25Kjv6enV+H/VQcj3r4dcimmI+eq41fB1d7EJIP8TzHoM618guWmvl1WGqMPvQO0AdcTfyCw0QEF4uDjs/ohZPx6xCyrzh+MynBxaJhNo7JxPw6JIz80uE+2uHcLE/ZOnRXbrjOTsvmPHw279hijkFLTNxetmVw/Jw+3jEot5d2SFzCteDkyj1xwiISve80KkL16JLF/CsG8edVqfDwcnVWe/yanJ+n4sVUQ8SkKFfSeZbEZiNSMBLLzdBIINoovMeGNN4I5NeD1Th75a+kW4oJ15tSttG9ytwDW5OOOylau6KQbPByJGrweJFskUhWlbQSIRW/DkdJ75fEamzk+FH7Gakcx834lqQIsmgMcjrJaIoyQ7Tlcvf9VqL1kQNVZpHhzKKwty9hJujdp1dIOIsjjEYgfLgGdUhC9RrciUjoyawoqYAQujnxQAfOE/qiZD6FAMLJ0gxby0Wjo9WX3ktFi+pyyFNFU1yXeHmDyDYOWPAEhrDSNULvKAXBo7JZFCyjZ/0J6FSgAiguqCkuOlEH2FIQyAwHtZeOy4anaertPLxaZ6fyLFz4zbgoCIHXDChIxT76oJCPgRB4SCUdAeCmfsvJQIgt1jVpw4++qGkJCihKDi/UUEw5RG9A37+cycAMxXSLzAPYcnOQeQDmH524ey8FzGhI+JLYGJ3E0H8CNf3icYr899kMgj8UEQdOGGUgDndlSjHkj1OJuOECF9xDCoF0RxMYkJi4xkcasszojQdFGNrRI/x/ra6+SwKJ+rH0ApL1MWcS3pDCXd3D/4V43CZNwhuQqQh74PAn1CMbqgEWGcGPCBv7QVIWO2DTDZl9OLA3c9cV/EWAcB+ehdy5iaGABc/+mQj77Jo7Lg+wx70xUAT+uYGbkH4BJ9/6Mhmgl0Bg6xO3DwRFhqDd73FsUjDRRJDh7Rd+mvEPBteO+NNDAfP1FP5of2meQ/BkcOR95vfB+kCKoJCPoQd1++Ph4OcmkZ74GMLC1PvkAk0LicfNyBD0wN+NOOsn5GQIf8TXPwMNC00pFydDMKZ9NeOQQHch6QQrwy+ozO9F+owgZYeoGo+VIfQRq2e/BlLYkGhZCawMwbD2WXWDBilVZM/LEIr2n4YpuOISdYGZIWgIHpYOeglk2gUzQ0jPeAxByNyTVVUyM4QcsofRB8IKOnWGmSGk2MT330OvgKDC+BfcDKHA6D4IgcqLim7LBjdDyJ7fY2xAYyNMh3IzhMzdXXMD8k2E+8PYGQIuS337LSQ7Eu4rYmcIyWg3oRfgf6Zrn5/hF5AVvo1DQCelTFXwMwSMfoY0T5pt4mcIuGXXJqB6dcLmNTAEfNNrjTtg70mb18AQGImdzQcEDNLyLg0MAbmmwH9FBg0MkQ8FRMiku2w1MAQs4h5svUf3HwENDIG8y08bXr/TSltcooEhkIaqPEjBIJIRf6GDISAqppBXSlsKrIMhUDwcQGsQbf2MDoaAV1NA4a9K9UXSOAL0x2e16N8alSwxMBgT6OOqiN18B2OpZGcB6XsLdO+gkhc1g6HXX5/uArGTUnBoBkMgRMwAM6JURGAIw/5yiBb4uZJUagjDftG0+fru/bmSzmYIw3697RvYP6K0c8QQhv0rygFQ9JVq9Qxh2J99iQGxVCn+NYRhf6Bbf/WHFkqPNoRhfzcqQMFRkhINYQgIijP4hvbPQ/vXUvvtof0+jf1+qf2xhf3xof0xvv06jf1am/16qf2at/15C/tzT/bnD2eQA7Y/j29/LYb99TT210TZX9c2g9pE++tL7a8Rtr/Oewa1+vbvt7B/z4z9+55msHfN/v2H9u8htX8f8Az2ctu/H9/+MxVmcC6G/WebzOB8GvvPGLL/nKgZnPVl/3lt9p+5N4NzE+0/+3IG55fafwYt8hH/t3OEweXf+rOgZ3CeN3b9x390JjtW/gvbxP/pXH1Uq7f+boQZ3G9h/x0lM7hnZgZ3Bdl/39MM7uyawb1r9t+dN4P7D+2/w3IG95DO4C7ZGdwHbP+dzjO4l3sGd6ujUcYVrb6RmiIR/RXDxEDc8HfQNVKRePcGeVP/CqjG/QE9IiMsG/4iHlxFKVhQf7DkV8MD0WwZJckLhwd/TkO04C1GThY4ffUAZ/JNQ/sCs3hFS1al+QZftIR2GF2ODub3n+FyeHGe1A5qgkJmKYqLkNo4pnigREhQbqB2HImq/K44yvGj2jEhM907XKg4HqFivHeQLXISRuOGPYWXUyK3Er+C0KcKhN7NH1bjFlZf6MD8ISb1NiKhj/rAcjPUiYo2Yv/lDw3pTmVhgP2K6pSrNx/lJ0EI+Poe6Q2UhPv0jPNKZZaUK0EE/w4WZ1HSajzBCYtIZCfTqAjBukgQhPvqnhHg8lQ/4vayLYPj55jyjkG5vbTya9gDB7aIJsVFRrRT52Z5ytahu3LDdXZaNuchr+uGHae4gEvFekBQ0ojBVzAbLGi4Apk/eJIeIxNCHVr0oAWHBnxLzBv4jr7CoVGilVCH6KFB9XpGjpQWsaBmMvIw5DQGMrDoJCIcpeO40ciI4whp+Ho47tlNIIJguB8ni92U/Dr4eG59LLKp+XWI3CHxgQxid6r59450ox7hieFszCn3/IGvKAEIsTZheL6hUNJZMFQnU7daRTle8SKHITKWRqTlesy6E69LoyYfAD/ZDfFa611i4NwDEeSZI/8xYyfLzdw9hiPyi3Dv4MtP5ezDwjd64gnhpUGRbN2sbb4PcV0tqjo+fDdt5m6TIkj5Q4Z/dqOL4t807BgAAAAASUVORK5CYII=" runat="server" CommandName="Insert" ToolTip="Insert" OnClick="lbInsert_Click" Width="20px" Height="20px"/>
                </FooterTemplate>
            </asp:TemplateField>
        </Columns>
        <FooterStyle BackColor="#CCCC99" />
        <HeaderStyle BackColor="#6B696B" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#F7F7DE" ForeColor="Black" HorizontalAlign="Right" />
        <RowStyle BackColor="#F7F7DE" />
        <SelectedRowStyle BackColor="#CE5D5A" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#FBFBF2" />
        <SortedAscendingHeaderStyle BackColor="#848384" />
        <SortedDescendingCellStyle BackColor="#EAEAD3" />
        <SortedDescendingHeaderStyle BackColor="#575357" />
    </asp:GridView>
    <asp:SqlDataSource ID="UsersSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:sklepConnectionString %>" ProviderName="<%$ ConnectionStrings:sklepConnectionString.ProviderName %>" SelectCommand="SELECT * FROM users" DeleteCommand="DELETE FROM users WHERE id = ?" UpdateCommand="UPDATE users SET username = ?, email = ?, password = ? WHERE id = ?" InsertCommand="INSERT INTO users (id, username, email, password) VALUES (?, ?, ?, ?)" OnInserting="insertusersql">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="password" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="username" Type="String" />
            <asp:Parameter Name="email" Type="String" />
            <asp:Parameter Name="password" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
</asp:Content>

