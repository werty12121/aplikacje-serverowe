﻿using Microsoft.AspNet.Identity;
using System;
using System.Linq;
using System.Web.UI;
using sklep;

public partial class Account_Register : Page
{
    protected void CreateUser_Click(object sender, EventArgs e)
    {
        if (IsValid)
        {
            Uzytkownik newuser =sklep.Uzytkownik.register(UserName.Text, Password.Text, Email.Text);
            if (newuser.ID != (-1).ToString() && newuser.ID != null)
            {
                Session["uzytkowniknumber"] = newuser.ID;
                Session["uzytkowniknazwa"] = newuser.Login;
            }
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);

        }
    }
}