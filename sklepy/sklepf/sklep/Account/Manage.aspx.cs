﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using sklep;

public partial class Account_Manage : System.Web.UI.Page
{
   
    

    protected void Page_Load()
    {
        if (Session["uzytkowniknumber"] == null)
        {
      
            Response.Redirect("~/");

        }
    }

    protected void ChangePassword_f(object sender, EventArgs e)
    {
        if (IsValid)
        {
            Uzytkownik temp = new Uzytkownik();
            temp.ID = Session["uzytkowniknumber"].ToString();
            temp.getupdate();
            if (temp.Password == CurrentPassword.Text)
                temp.Password = NewPassword.Text;
            temp.setupdate();
        }
    }
}