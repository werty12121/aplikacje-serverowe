﻿using sklep;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class produkt : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["id"] == null)
        {
            IdentityHelper.RedirectToReturnUrl(Request.QueryString["ReturnUrl"], Response);
        }
        sklep.Produkt p = new sklep.Produkt();
        p.ID = Request.QueryString["id"];
        p.getupdate();
        Name.Text = p.name;
        Descripction.Text = p.descripction;
        Price.Text = p.price;
        Quantity.Text = p.quantity;
        Producent.Text = p.producent;
        ProductImage.ImageUrl = p.img;
    }
}