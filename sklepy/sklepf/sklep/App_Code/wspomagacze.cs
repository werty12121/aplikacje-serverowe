﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Opis podsumowujący dla wspomagacze
/// </summary>
namespace sklep
{
    public class Produkt
    {
        public string ID, quantity, price, img, name, descripction, producent;
        private SQLConnector con;
        public Produkt()
        {
            con = new SQLConnector();
        }
        public void insert()
        {
            this.ID = con.insert("INSERT INTO products (quantity, price, img, name, descripction, producent) VALUES ('" + this.quantity + "', '" + this.price + "', '" + this.img + "', '" + this.name + "', '" + this.descripction + "', '" + this.producent+ "')").ToString();
            //con.insert(this.ID.ToString(), this.Login, this.Password);
        }
        public void sendupdate()
        {
            con.justquery("UPDATE products SET quantity='" + this.quantity + "', price='" + this.price + "', img='" + this.img + "', name='" + this.name + "', descripction='" + this.descripction + "', producent='" + this.producent + "' WHERE id='" + this.ID + "'");
        }
        public void getupdate()
        {
            MySqlDataReader reader = con.getreader("SELECT * FROM products WHERE id='" + this.ID + "'");
            if (reader.Read())
            {
                this.quantity = reader["quantity"].ToString();
                this.price =reader["price"].ToString();
                this.img = reader["img"].ToString();
                this.name = reader["name"].ToString();
                this.descripction = reader["descripction"].ToString();
                this.producent = reader["producent"].ToString();
            }
        }
        public static Produkt getById(string Id)
        {
            Produkt resoult = new Produkt();
            resoult.ID = Id;
            resoult.getupdate();
            return resoult;
        }
    }
    public partial class SQLConnector
    {
        private string myconnection =
              "SERVER=localhost;" +
              "DATABASE=sklep;" +
              "UID=root;" +
              "PASSWORD=;";
        MySqlConnection connect()
        {
            MySqlConnection connection = new MySqlConnection(myconnection);
            try
            {
                connection.Open();
                Console.WriteLine("Connected");
                return connection;
            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }
        public void justquery(string Query)
        {
            MySqlConnection conn = this.connect();
            if (conn == null) return;
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;
            command.ExecuteNonQuery();
            return;
        }
        public int insert(string Query)
        {

            MySqlConnection conn = this.connect();
            if (conn == null) return -1;//error
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;
            try
            {
                command.ExecuteNonQuery();
            }
            catch (Exception e)
            {
                return -1;//error
            }

            return (int)command.LastInsertedId;

        }

        public MySqlDataReader getreader(string Query)
        {
            MySqlConnection conn = connect();
            if (conn == null) return null;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = Query;
            MySqlDataReader reader = command.ExecuteReader();

            return reader;
        }
    }
    public class Uzytkownik
    {
        public string ID, Login, Password, Email;
        private SQLConnector con;
        public Uzytkownik()
        {
            con = new SQLConnector();
        }
        public void insert()
        {
            this.ID = con.insert("INSERT INTO users (username, password, email) VALUES ('" + this.Login + "', '" + this.Password + "', '" + this.Email + "') ").ToString();

            //con.insert(this.ID.ToString(), this.Login, this.Password);
        }
        public void setupdate()
        {
            con.justquery("UPDATE users SET username='" + this.Login + "', password='" + this.Password + "', email='" + this.Email + "' WHERE id='" + this.ID + "'");
        }
       
        public void getupdate()
        {
            MySqlDataReader reader = con.getreader("SELECT * FROM users WHERE id='" + this.ID + "'");
            while (reader.Read())
            {
                this.Login = reader["username"].ToString();
                this.Password =reader["password"].ToString();
                this.Email =reader["email"].ToString();
            }
        }
        public static Uzytkownik loguj(string Login, string Password)
        {
            SQLConnector conn = new SQLConnector();
            Uzytkownik temp = new Uzytkownik();
            MySqlDataReader reader = conn.getreader("SELECT * FROM users WHERE password='" + Password + "' AND username='" + Login + "'");
            if (!reader.Read()) {
                return null;
            }
            temp.ID = reader["id"].ToString();
            temp.getupdate();
            return temp; 
        }
        public static Uzytkownik register(string username, string Password, string Email) {
            Uzytkownik newuser = new Uzytkownik();
            newuser.Login = username;
            newuser.Password = Password;
            newuser.Email = Email;
            newuser.insert();
            return newuser;
        }
        public static Uzytkownik getById(string Id)
        {
            Uzytkownik resoult = new Uzytkownik();
            resoult.ID = Id;
            resoult.getupdate();
            return resoult;
        }
    }
}