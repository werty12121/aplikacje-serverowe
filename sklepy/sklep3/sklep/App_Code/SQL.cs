﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace sql
{
    public partial class SQL
    {
        public SQL()
        {
            //
            // TODO: Tutaj dodaj logikę konstruktora
            //
        }
        MySqlConnection connect()
        {
            string myconnection =
               "SERVER=localhost;" +
               "DATABASE=projekt;" +
               "UID=root;" +
               "PASSWORD=;";

            MySqlConnection connection = new MySqlConnection(myconnection);

            try
            {

                connection.Open();
                Console.WriteLine("Connected");
                return connection;

            }
            catch (MySqlException ex)
            {
                Console.WriteLine("Error");
            }
            return null;
        }
        public void zapytanie(string Query)
        {
            MySqlConnection conn = this.connect();
            if (conn == null) return;
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;
            command.ExecuteNonQuery();
            conn.Close();
            return;
        }
        public int insert(string Query)
        {

            MySqlConnection conn = this.connect();
            if (conn == null) return -1;
            MySqlCommand command = conn.CreateCommand();
            command.CommandText = Query;
            try
            {
                command.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception e) {
                return -1;
            }
            
            return (int)command.LastInsertedId;
            
        }

        public MySqlDataReader reader(string Query)
        {
            MySqlConnection conn = connect();
            if (conn == null) return null;
            MySqlCommand command = conn.CreateCommand();

            command.CommandText = Query;
            MySqlDataReader reader = command.ExecuteReader();
            
            return reader;
        }
    }
}
