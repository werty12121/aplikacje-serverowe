﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <div class="row">
        <br />
         <br />
         <br />
        <asp:GridView ID="GridView1" AutoGenerateColumns="False" runat="server" AllowSorting="True" DataSourceID="SqlDataSource3" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" ForeColor="Black" GridLines="Horizontal">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <img src=<%# Eval("obrazek") %>/>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="nazwa" SortExpression="nazwa">
                    <ItemTemplate>
                        <asp:Label Text="Nazwa produktu: " runat="server"></asp:Label>
                        <asp:Label ID="l1" runat="server" Text='<%# Bind("nazwa") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="producent" SortExpression="producent">
                     <ItemTemplate>
                          <asp:Label Text="Producent produktu: " runat="server"></asp:Label>
                    <asp:Label ID="l2" runat="server" Text='<%# Bind("producent") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderText="cena" SortExpression="cena">
                     <ItemTemplate>
                          <asp:Label Text="Cena produktu: " runat="server"></asp:Label>
                    <asp:Label ID="l3" runat="server" Text='<%# Bind("cena") %>'></asp:Label>
                </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button Text="kup" OnClick="kup_click" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
            <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
            <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F7F7F7" />
            <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
            <SortedDescendingCellStyle BackColor="#E5E5E5" />
            <SortedDescendingHeaderStyle BackColor="#242121" />
        </asp:GridView>

        <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:koszalkaConnectionString %>" ProviderName="<%$ ConnectionStrings:koszalkaConnectionString.ProviderName %>" SelectCommand="SELECT nazwa, producent, cena, obrazek FROM produkty"></asp:SqlDataSource>
    
    </div>
      
    
    
       











</asp:Content>
