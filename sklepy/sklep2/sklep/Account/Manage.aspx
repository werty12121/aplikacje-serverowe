﻿<%@ Page Title="Zakontem" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Manage.aspx.cs" Inherits="Account_Manage" %>



<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    

    

    <div class="row">
        <div class="col-md-12">
            <section id="dataedit">
                 <asp:PlaceHolder runat="server" ID="changePasswordHolder" Visible="true">
                    <div class="form-horizontal">
                        <h4>Formularz zmiany hasła</h4>
                        <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" ID="CurrentPasswordLabel" AssociatedControlID="CurrentPassword" CssClass="col-md-2 control-label">Bieżące hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="CurrentPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="CurrentPassword"
                                    CssClass="text-danger" ErrorMessage="Pole bieżącego hasła jest wymagane."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="NewPasswordLabel" AssociatedControlID="NewPassword" CssClass="col-md-2 control-label">Nowe hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="NewPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="NewPassword"
                                    CssClass="text-danger" ErrorMessage="Nowe hasło jest wymagane."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" ID="ConfirmNewPasswordLabel" AssociatedControlID="ConfirmNewPassword" CssClass="col-md-2 control-label">Potwierdź nowe hasło</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="ConfirmNewPassword" TextMode="Password" CssClass="form-control" />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Potwierdzenie nowego hasła jest wymagane."
                                    ValidationGroup="ChangePassword" />
                                <asp:CompareValidator runat="server" ControlToCompare="NewPassword" ControlToValidate="ConfirmNewPassword"
                                    CssClass="text-danger" Display="Dynamic" ErrorMessage="Nowe hasło i jego potwierdzenie są niezgodne."
                                    ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" Text="Zmień hasło" OnClick="ChangePassword_Click" CssClass="btn btn-default" ValidationGroup="ChangePassword" />
                            </div>
                        </div>
                    </div>
                </asp:PlaceHolder>

                <asp:PlaceHolder runat="server" ID="updateadres" Visible="true">

                    <div class="form-horizontal">
                        <h4>Zmiana adresu</h4>
                        <hr />
                        <asp:ValidationSummary runat="server" ShowModelStateErrors="true" CssClass="text-danger" />
                        <div class="form-group">
                            <asp:Label runat="server" AssociatedControlID="adres" CssClass="col-md-2 control-label">Adres</asp:Label>
                            <div class="col-md-10">
                                <asp:TextBox runat="server" ID="adres"  CssClass="form-control"  />
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="adres"
                                    CssClass="text-danger" ErrorMessage="Adres jest wymagany."
                                    Display="Dynamic" ValidationGroup="adresvgroup" />
                                <asp:ModelErrorMessage runat="server" ModelStateKey="adres" AssociatedControlID="adres"
                                    CssClass="text-danger" SetFocusOnError="true" />
                            </div>
                        </div>

                    
                        </div>

                        <div class="form-group">
                            <div class="col-md-offset-2 col-md-10">
                                <asp:Button runat="server" Text="Zmień adres na nowy" ValidationGroup="adresvgroup" OnClick="updateadres_Click" CssClass="btn btn-default" />
                            </div>
                        </div>
                    
                </asp:PlaceHolder>

            </section>
        </div>
    </div>
    

</asp:Content>
