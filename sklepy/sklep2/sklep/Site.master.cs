﻿using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SiteMaster : MasterPage
{

    protected void Page_Init(object sender, EventArgs e)
    {
       
    }

    protected void master_Page_PreLoad(object sender, EventArgs e)
    {
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["id"] == null)
        {
            zalogowany.Visible = false;
            niezalogowany.Visible = true;
        }
        else {
            zalogowany.Visible = true;
            niezalogowany.Visible = false;
        }
        
    }

    protected void Logout(object sender, EventArgs e)
    {
        try
        {
            Session["id"] = null;
            Session["login"] = null;
            Response.Redirect("~/");
        }
        catch (Exception ex) { }
       
    }
}