﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Admin_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" Runat="Server">
    <div class="row">
    <asp:Label ID="Label1" runat="server" Text="Uzytkownicy:"></asp:Label>
    <asp:GridView DataKeyNames="id" ID="Uzytkownicygv" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource1" ForeColor="Black" GridLines="Horizontal" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
      
    </asp:GridView>
   
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:koszalkaConnectionString %>" DeleteCommand="DELETE FROM uzytkownicy WHERE id = ?" InsertCommand="INSERT INTO uzytkownicy (id, login, haslo, adres) VALUES (?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:koszalkaConnectionString.ProviderName %>" SelectCommand="SELECT * FROM uzytkownicy" UpdateCommand="UPDATE uzytkownicy SET login = ?, haslo = ?, adres = ? WHERE id = ?">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="login" Type="String" />
            <asp:Parameter Name="haslo" Type="String" />
            <asp:Parameter Name="adres" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="login" Type="String" />
            <asp:Parameter Name="haslo" Type="String" />
            <asp:Parameter Name="adres" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>

        <asp:Label Text="login" runat="server"></asp:Label><asp:TextBox ID="login" runat="server"></asp:TextBox><br />
        <asp:Label Text="haslo" runat="server"></asp:Label><asp:TextBox ID="haslo" runat="server"></asp:TextBox><br />
        <asp:Label Text="adres" runat="server"></asp:Label><asp:TextBox ID="adres" runat="server"></asp:TextBox><br />
        <asp:Button ID="dodaj_uzytkownika" runat="server" Text="Dodaj uzytkownika" OnClick="dodaj_uzytkownika_Click" />
   </div>
    <div class="row">
    <asp:Label ID="Label2" runat="server" Text="Produkty:"></asp:Label>
    <asp:GridView  DataKeyNames="id" ID="produktygv" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="4" DataSourceID="SqlDataSource2" ForeColor="Black" GridLines="Horizontal" AllowSorting="True">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
        </Columns>
        <FooterStyle BackColor="#CCCC99" ForeColor="Black" />
        <HeaderStyle BackColor="#333333" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Right" />
        <SelectedRowStyle BackColor="#CC3333" Font-Bold="True" ForeColor="White" />
        <SortedAscendingCellStyle BackColor="#F7F7F7" />
        <SortedAscendingHeaderStyle BackColor="#4B4B4B" />
        <SortedDescendingCellStyle BackColor="#E5E5E5" />
        <SortedDescendingHeaderStyle BackColor="#242121" />
    </asp:GridView>
        <asp:Label Text="nazwa" runat="server"></asp:Label><asp:TextBox ID="nazwa" runat="server"></asp:TextBox><br />
        <asp:Label Text="producent" runat="server"></asp:Label><asp:TextBox ID="producent" runat="server"></asp:TextBox><br />
        <asp:Label Text="cena" runat="server"></asp:Label><asp:TextBox ID="cena" runat="server"></asp:TextBox><br />
         <asp:Label Text="adres do obrazka" runat="server"></asp:Label><asp:TextBox ID="obrazek" runat="server"></asp:TextBox><br />
        <asp:Button ID="dodajProdukt" runat="server" Text="Dodaj Produkt" OnClick="dodajProdukt_Click" />
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:koszalkaConnectionString %>" DeleteCommand="DELETE FROM produkty WHERE id = ?" InsertCommand="INSERT INTO produkty (id, nazwa, cena, producent, obrazek) VALUES (?, ?, ?, ?, ?)" ProviderName="<%$ ConnectionStrings:koszalkaConnectionString.ProviderName %>" SelectCommand="SELECT * FROM produkty" UpdateCommand="UPDATE produkty SET nazwa = ?, cena = ?, producent = ?, obrazek = ? WHERE id = ?">
        <DeleteParameters>
            <asp:Parameter Name="id" Type="Int32" />
        </DeleteParameters>
        <InsertParameters>
            <asp:Parameter Name="id" Type="Int32" />
            <asp:Parameter Name="nazwa" Type="String" />
            <asp:Parameter Name="cena" Type="Double" />
            <asp:Parameter Name="producent" Type="String" />
            <asp:Parameter Name="obrazek" Type="String" />
        </InsertParameters>
        <UpdateParameters>
            <asp:Parameter Name="nazwa" Type="String" />
            <asp:Parameter Name="cena" Type="Double" />
            <asp:Parameter Name="producent" Type="String" />
            <asp:Parameter Name="obrazek" Type="String" />
            <asp:Parameter Name="id" Type="Int32" />
        </UpdateParameters>
    </asp:SqlDataSource>
   </div>
</asp:Content>

