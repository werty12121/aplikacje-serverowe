-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 13 Lis 2018, 22:15
-- Wersja serwera: 10.1.31-MariaDB
-- Wersja PHP: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `koszalka`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `produkty`
--

CREATE TABLE `produkty` (
  `id` int(11) NOT NULL,
  `nazwa` varchar(256) NOT NULL,
  `cena` double NOT NULL,
  `producent` varchar(256) NOT NULL,
  `obrazek` varchar(12000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `produkty`
--

INSERT INTO `produkty` (`id`, `nazwa`, `cena`, `producent`, `obrazek`) VALUES
(1, 'asdasd', 12, 'asdasd', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(2, 'asdasdasdasd', 1232, 'asdasdasdasd', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(4, '123123', 234324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(5, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(6, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(7, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(8, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(9, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(10, 'sdf', 234234, 'sdf', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(11, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(12, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(13, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(19, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(20, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(21, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(22, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png'),
(23, 'werwerwer', 324324, 'werwer', 'http://localhost/phpmyadmin/themes/pmahomme/img/logo_left.png');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `uzytkownicy`
--

CREATE TABLE `uzytkownicy` (
  `id` int(11) NOT NULL,
  `login` varchar(256) NOT NULL,
  `haslo` varchar(256) NOT NULL,
  `adres` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Zrzut danych tabeli `uzytkownicy`
--

INSERT INTO `uzytkownicy` (`id`, `login`, `haslo`, `adres`) VALUES
(9, 'admin', 'admin', 'admin');

--
-- Indeksy dla zrzutów tabel
--

--
-- Indeksy dla tabeli `produkty`
--
ALTER TABLE `produkty`
  ADD PRIMARY KEY (`id`);

--
-- Indeksy dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `produkty`
--
ALTER TABLE `produkty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT dla tabeli `uzytkownicy`
--
ALTER TABLE `uzytkownicy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
