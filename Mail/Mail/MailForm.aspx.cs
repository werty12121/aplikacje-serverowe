﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net.Mail;
using System.Net;
using System.Collections;
using System.IO;

namespace Mail
{
    public partial class MailForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void bSend_Click(object sender, EventArgs e)
        {
            SmtpClient client;
            MailMessage message;
            ArrayList attachemtList = new ArrayList();
            try
            {
                if(rbLocalhost.SelectedValue == "localhost")
                {
                    message = new MailMessage(tbFrom.Text, tbTo.Text);
                    message.Subject = tbSubject.Text;
                    message.Body = tbText.Text;
                    client = new SmtpClient(tbServer.Text);
                    client.Credentials = CredentialCache.DefaultNetworkCredentials;
                    for (int i = 0; i < lb1.Items.Count; i++)
                    {
                        Attachment attachment = new Attachment(Server.MapPath("~/") + lb1.Items[i].ToString());
                        message.Attachments.Add(attachment);
                        attachemtList.Add(attachment);
                    }
                    client.Send(message);
                    lInfo1.Text = "Message sent";
                    for (int j = 0; j < attachemtList.Count; j++)
                    {
                        ((Attachment)attachemtList[j]).Dispose();
                    }
                    for (int i = 0; i < lb1.Items.Count; i++)
                    {
                        File.Delete(Server.MapPath("~/") + lb1.Items[i].ToString());
                    }
                    lb1.Items.Clear();
                }

                else
                {
                    message = new MailMessage(tbFrom.Text, tbTo.Text);
                    message.Subject = tbSubject.Text;
                    message.Body = tbText.Text;
                    client = new SmtpClient(tbServer.Text, int.Parse(tbPort.Text));
                    client.UseDefaultCredentials = false;
                    client.EnableSsl = true;
                    client.Credentials = new System.Net.NetworkCredential(tbUser.Text, tbPassword.Text);
                    for (int i = 0; i < lb1.Items.Count; i++)
                    {
                        Attachment attachment = new Attachment(Server.MapPath("~/") + lb1.Items[i].ToString());
                        message.Attachments.Add(attachment);
                        attachemtList.Add(attachment);
                    }
                    client.Send(message);
                    lInfo1.Text = "Message sent";
                    for (int j = 0; j < attachemtList.Count; j++)
                    {
                        ((Attachment)attachemtList[j]).Dispose();
                    }
                    for (int i = 0; i < lb1.Items.Count; i++)
                    {
                        File.Delete(Server.MapPath("~/") + lb1.Items[i].ToString());
                    }
                    lb1.Items.Clear();
                }
                
            }
            catch(Exception ex)
            {
                lInfo1.Text="You can not send ("+ ex.Message +")";
            }
        }

        protected void bClear_Click(object sender, EventArgs e)
        {
            tbFrom.Text = "";
            tbTo.Text = "";
            tbSubject.Text = "";
            tbText.Text = "";
            tbServer.Text = "";
            lInfo1.Text = "";
            lb1.Items.Clear();
        }

        protected void bSave_Click(object sender, EventArgs e)
        {
            if (fu1.HasFile)
            {
                string fileName = fu1.FileName;
                string serverPath = Server.MapPath("~/") + fileName;
                fu1.SaveAs(serverPath);
                lb1.Items.Add(fileName);
                lInfo2.Text = "Attachment downloaded";
            }
        }

        protected void rbLocalhost_SelectedIndexChanged(object sender, EventArgs e)
        {
            lPort.Visible = false;
            tbPort.Visible = false;
            lUser.Visible = false;
            tbUser.Visible = false;
            lPassword.Visible = false;
            tbPassword.Visible = false;
            if (rbLocalhost.SelectedValue == "localhost")
            {
                lPort.Visible = false;
                tbPort.Visible = false;
                lUser.Visible = false;
                tbUser.Visible = false;
                lPassword.Visible = false;
                tbPassword.Visible = false;
            }
            else
            {
                lPort.Visible = true;
                tbPort.Visible = true;
                lUser.Visible = true;
                tbUser.Visible = true;
                lPassword.Visible = true;
                tbPassword.Visible = true;
            }
        }
    }
}