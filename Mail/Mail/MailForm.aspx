﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MailForm.aspx.cs" Inherits="Mail.MailForm" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        #form1 {
            height: 204px;
        }
        .auto-style1 {
            text-align: center;
        }
        .auto-style2 {
            width: 100%;
        }
        .auto-style3 {
            height: 23px;
        }
        .auto-style5 {
            width: 141px;
        }
        .auto-style6 {
            height: 23px;
            width: 141px;
        }
        .auto-style8 {
            width: 558px;
        }
        .auto-style9 {
            height: 23px;
            width: 558px;
        }
        .auto-style11 {
            height: 330px;
        }
        .auto-style12 {
            height: 49px;
            width: 141px;
        }
        .auto-style13 {
            height: 49px;
            width: 558px;
        }
        .auto-style14 {
            height: 49px;
        }
        .auto-style15 {
            height: 152px;
            width: 141px;
        }
        .auto-style16 {
            height: 152px;
            width: 558px;
        }
        .auto-style17 {
            height: 152px;
        }
        .auto-style18 {
            width: 141px;
            height: 146px;
        }
        .auto-style19 {
            width: 558px;
            height: 146px;
        }
        .auto-style20 {
            height: 146px;
        }
        .auto-style21 {
            margin-left: 0px;
        }
        .auto-style22 {
            margin-top: 0px;
        }
        .auto-style23 {
            width: 141px;
            height: 69px;
        }
        .auto-style24 {
            width: 558px;
            height: 69px;
        }
        .auto-style25 {
            height: 69px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" class="auto-style11">
        <div class="auto-style1">
                <table class="auto-style2">
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td class="auto-style8">
                            <asp:Button ID="bClear" runat="server" Text="Clear" OnClick="bClear_Click" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style5">From</td>
                        <td class="auto-style8">
                            <asp:TextBox ID="tbFrom" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style5">To</td>
                        <td class="auto-style8">
                            <asp:TextBox ID="tbTo" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style5">Subject</td>
                        <td class="auto-style8">
                            <asp:TextBox ID="tbSubject" runat="server"></asp:TextBox>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style18">Text</td>
                        <td class="auto-style19">
                            <asp:TextBox ID="tbText" runat="server" Height="114px" TextMode="MultiLine" Width="270px"></asp:TextBox>
                        </td>
                        <td class="auto-style20"></td>
                    </tr>
                    <tr>
                        <td class="auto-style12">Server SMTP</td>
                        <td class="auto-style13">
                            <asp:TextBox ID="tbServer" runat="server" CssClass="auto-style22"></asp:TextBox>
                            <asp:Label ID="lPort" runat="server" Text="Port:"></asp:Label>
                            <asp:TextBox ID="tbPort" runat="server"></asp:TextBox>
                        </td>
                        <td class="auto-style14">
                            <asp:RadioButtonList ID="rbLocalhost" runat="server" CssClass="auto-style21" Width="122px" AutoPostBack="True" OnSelectedIndexChanged="rbLocalhost_SelectedIndexChanged">
                                <asp:ListItem Selected="True">localhost</asp:ListItem>
                                <asp:ListItem>authenticated</asp:ListItem>
                            </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style23">
                            <asp:Label ID="lUser" runat="server" Text="User:"></asp:Label>
                            <br />
                            <asp:Label ID="lPassword" runat="server" Text="Password:"></asp:Label>
                        </td>
                        <td class="auto-style24">
                            
                            <asp:TextBox ID="tbUser" runat="server"></asp:TextBox>
                            <br />
                            <asp:TextBox ID="tbPassword" runat="server"></asp:TextBox>
                            
                        </td>
                        <td class="auto-style25">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td class="auto-style8">
                            <asp:Button ID="bSend" runat="server" Text="Send" OnClick="bSend_Click" />
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td class="auto-style8">
                            <asp:Label ID="lInfo1" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style6">Attachemnts</td>
                        <td class="auto-style9">
                            <asp:FileUpload ID="fu1" runat="server" />
                        </td>
                        <td class="auto-style3">
                            <asp:Button ID="bSave" runat="server" Text="Save" OnClick="bSave_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style15"></td>
                        <td class="auto-style16">
                            <asp:ListBox ID="lb1" runat="server" Height="90px" Width="203px"></asp:ListBox>
                        </td>
                        <td class="auto-style17"></td>
                    </tr>
                    <tr>
                        <td class="auto-style5">&nbsp;</td>
                        <td class="auto-style8">
                            <asp:Label ID="lInfo2" runat="server"></asp:Label>
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
                </div>

    </form>
</body>
</html>
